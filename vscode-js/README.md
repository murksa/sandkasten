# JavaScript with VS Code

## Testing

### Vanilla

As soon as there is a 'test' entry in the 'scripts' section of your package.json, you are able to run and debug tests for your project by one of the following
* hitting Ctrl-F5 and F5 (as long as a 'start' entry is missing; I couldn't find out by now which one is run if both are present)
* clicking 'Debug' above the scripts section in 'package.json'
* using the 'npm scripts' section within Explorer (enabled using the ... menu at the top of Explorer)

### Mocha Test Explorer

You probably want to be able to selectively run a group of tests or a single test.

* Install 'Mocha Test Explorer' extension (which will install the required extensions 'Test Explorer UI' and 'Test Adapter Converter'); a new icon in the activity bar at the left occurs
* Make sure the test folder within your project is 'test' (otherwise Mocha will not find the test files); a 'test' script is not required for the extension on the other hand
* To configure a different pattern for test files for Mocha Test Explorer, add the following to your user settings file:
```
"mochaExplorer.files": ["test/**/*.js", "**/*-test.js"]
```
Note: For some reason, the globs `**/*test.js` and `**/*.test.js` will cause a 'Mocha: Error'.

# npm scripts

https://docs.npmjs.com/cli/v8/using-npm/scripts

* `npm start` and `npm run start` both execute `npm run prestart`, `npm run start`, `npm run poststart`.
  Try it with 
```
{
  "name": "demonstrate-npm-start",
  "scripts": {
    "prestart": "echo prestart",
    "start": "echo start",
    "poststart": "echo poststart"
  }
}
```
* `npm test` is handeled the same way 

