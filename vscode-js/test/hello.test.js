const {expect} = require('chai');
const hello = require('../src/hello')

describe('hello', () => {
    
    it('exists', () => {
        expect(hello).to.be.a('function');
    })

    it('says hello', () => {
        expect(hello()).to.eq('hello');
    })

    it('says not huhu', () => {
        expect(hello()).to.not.eq('huhu');
    })

})
