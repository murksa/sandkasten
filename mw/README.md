# Mediawiki with some extensions

* Quickly up and running; for testing purposes only!
* Uses SQLite, data stored in `./data` folder
* Extensions required in `./config/composer.local.json`
* and enabled in `./config/LocalSettings.php`

## Installation

Folder `.data` must be empty. Then
```
make install
```

## Update 

After change to `composer.local.json` to add extension, first execute
```
make update
```
Then add corresponding `wfLoadExtension()` to `LocalSettings.php`.

## Bash

To bash into the container:
```
make bash
```

## Todo

* job runner