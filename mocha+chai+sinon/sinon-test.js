const {assert, expect} = require('chai');
const should = require('chai').should();
const sinon = require('sinon');
const fs = require('fs');

describe('sinon', () => {

    afterEach(() => {
        sinon.restore();
    });

    describe('odds', () => {
        it('fake and stub throw differently if passed a string', () => {
            try { sinon.fake.throws('message')() }
            catch (e) {
                expect(e).to.be.a('Error');
                expect(e.message).to.eq('message');
            }

            // This is not what you expect: passing a string to throws defines the
            // type of the error, not the message! This is different to how fake works!
            try { sinon.stub().throws('message')() }
            catch (e) {
                expect(e).to.be.a('Error');
                expect(e.name).to.eq('message');
                expect(e.message).to.eq('');
            }

            // In particular, you cannot expect the message:
            // expect(sinon.stub().throws('message')).to.throw('message');
            // Instead, you get
            expect(sinon.stub().throws('message')).to.throw('');
        })
    });

    it('anonymouns spy', () => {
        let spy = sinon.spy();
        
        spy(1);
        spy(1, 2);

        assert.isTrue(spy.calledTwice);
        assert.isTrue(spy.calledWith(1, 2));
        // expect(spy.calledOnce).true;

        spy.getCalls().map(c => c.args).should.eql([[1], [1, 2]]);

    })

    it('wrapping spy 1', () => {
        let service = {
            add: (x, y) => x + y,
            multiply: (x, y) => x * y
        }
        
        let spy = sinon.spy(service);
        
        service.add(1, 2);

        assert.isTrue(spy.add.calledWithExactly(1, 2));
        assert.isTrue(service.add.calledWithExactly(1, 2));

    })

    it('wrapping spy 2', () => {
       
        let spy = sinon.spy({log: () => null});
        
        spy.log("1");
        spy.log("2");
        spy.log("3");

        spy.log.getCalls().flatMap(c => c.args)
            .should.eql(['1', '2', '3']);

        spy.log.getCall(0).args[0].should.equal("1");

        assert.equal(spy.log.getCall(0).args[0], '1');
    })

    describe('stubs', () => {

        it('throws Error', () => {
            sinon.stub(fs, 'readFileSync').throws(new Error('message'));

            expect(() => fs.readFileSync('x')).to.throw(Error, 'message');
        })

        it('withArgs 1', () => {
            sinon.stub(fs, 'readFileSync');

            const result = fs.readFileSync('x');
            
            expect(result).to.eq(undefined);
        })

        it('withArgs 2', () => {
            const stub = sinon.stub(fs, 'readFileSync');
            stub.throws(new Error('unexpected call'));
            stub.withArgs('x').returns('y');

            const result = fs.readFileSync('x');

            expect(result).to.eq('y');
            expect(fs.readFileSync).to.throw('unexpected call');
        })

        it('withArgs 2a', () => {
            sinon.stub(fs, 'readFileSync')
                .throws(new Error('unexpected call'))
                .withArgs('x').returns('y');

            const result = fs.readFileSync('x');

            expect(result).to.eq('y');
            expect(fs.readFileSync).to.throw('unexpected call');
        })

    })

});



