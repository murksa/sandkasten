const fs = require('fs');

function readConfiguration(path) {
    return path
        ? JSON.parse(fs.readFileSync(path))
        : {
            verbose: false,
            url: 'http://localhost'
        }
}

const { expect } = require('chai')
const sinon = require('sinon')


describe('readConfiguration', () => {

    it('should return default if not given a file name', () => {
        const result = readConfiguration();
        expect(result).to.eql({ verbose: false, url: 'http://localhost' });
    });

    it('should return configuration from file', () => {
        const path = './test.cfg';
        const cfg = { verbose: true, url: "http://example.com/api" };
        sinon.stub(fs, 'readFileSync')
            .withArgs(path).returns(JSON.stringify(cfg))
            
            .throws();
        
        const x = fs.readFileSync('x');
        console.dir('x = ' + x);
        return;

        const result = readConfiguration('x');
        
        expect(result).to.eql(cfg);
    });
});

