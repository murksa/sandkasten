const { assert, expect } = require('chai');
const should = require('chai').should();
const sinon = require('sinon');

describe('chai', () => {

    afterEach(() => {
        sinon.restore();
    })

    it('assert', () => {
        assert.equal(1, 1);
    })

    it('expect', () => {
        expect(1).to.equal(1);
        expect(1).to.be.a('number');
        expect(1).to.not.equal(2);
        expect({ x: 0 }).to.not.equal({ x: 0 });
        expect({ x: 0 }).to.eql({ x: 0 });
    })

    it('should', () => {
        (1).should.equal(1);
        (1).should.be.a('number');
        (1).should.not.equal(2);
        ({ x: 0 }).should.not.equal({ x: 0 });
        ({ x: 0 }).should.eql({ x: 0 });
    })

    it('throw', () => {
        expect(() => { throw new Error('message') }).to.throw(Error, 'message');
        expect(() => { throw 'message' }).to.throw('message');
    });
    
});
